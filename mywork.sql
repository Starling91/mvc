-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 20 2016 г., 07:44
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `mywork`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category_products`
--

CREATE TABLE IF NOT EXISTS `category_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `category_products`
--

INSERT INTO `category_products` (`id`, `title`, `status`) VALUES
(1, 'Для туризма', 'На складе'),
(2, 'Продукты питания', '1'),
(3, 'Игрушки', '1'),
(4, 'Мебель', '1'),
(5, 'Уценка', '1'),
(6, 'Пресса', '1'),
(7, 'Парфюмерия', '1'),
(8, 'Спортивный инвентарь', '1'),
(9, 'Одежда', '1'),
(10, 'Техника', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `composition_order`
--

CREATE TABLE IF NOT EXISTS `composition_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `composition_order`
--

INSERT INTO `composition_order` (`id`, `id_order`, `id_product`, `price`, `count`) VALUES
(1, 1, 2, 14000, 2),
(2, 5, 3, 400, 4),
(3, 1, 4, 10000, 1),
(4, 5, 6, 300, 1),
(5, 10, 9, 150, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `Orders`
--

CREATE TABLE IF NOT EXISTS `Orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `date_order` date NOT NULL,
  `status` enum('Принят','Обработан','Впроцессе','Доставлен') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `Orders`
--

INSERT INTO `Orders` (`id`, `id_user`, `date_order`, `status`) VALUES
(1, 1, '2016-02-16', 'Доставлен'),
(2, 6, '2015-09-27', 'Доставлен'),
(3, 5, '2015-10-14', 'Обработан'),
(4, 10, '2016-01-29', 'Впроцессе'),
(5, 8, '2016-01-31', 'Впроцессе'),
(6, 7, '2016-02-08', 'Доставлен'),
(7, 4, '2016-02-26', 'Принят'),
(8, 3, '2016-02-14', 'Принят'),
(9, 2, '2016-02-10', 'Впроцессе'),
(10, 9, '2016-02-08', 'Обработан');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `id_catalog` int(25) NOT NULL,
  `title` varchar(30) NOT NULL,
  `mark` varchar(30) NOT NULL,
  `count` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `description` text NOT NULL,
  `status` enum('Нет в наличии','Есть в наличии','','') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `id_catalog`, `title`, `mark`, `count`, `price`, `description`, `status`) VALUES
(1, 1, 'Супер крутая палатка', 'Guccie', 5, 100, 'Ошень просто ошень классная полаточка))', 'Есть в наличии'),
(2, 2, 'Сок апельсиновый', 'Добрый', 1, 150, 'Сок добрый-"Как моя бабушка"', 'Нет в наличии'),
(3, 3, 'Солдатик-"Русский"', 'НАШ', 5, 400, 'Солдатик русский обыкновенный,развивает память и творческое мышление', 'Есть в наличии'),
(4, 4, 'Кровать', 'IKEA', 1, 10000, 'Ну очень дешевая кровать', 'Есть в наличии'),
(5, 5, 'Уценка -Стол', 'IKEA', 1, 4500, 'Стол уценен,он уже  столько к нам возвращался обратно что вы не представляете!!', 'Есть в наличии'),
(6, 6, 'Журнал PlayBoy', 'PlayBoy', 0, 300, 'Журнал для взрослыx!', 'Нет в наличии'),
(7, 7, 'Туалетное вода((', 'no', 100, 800, 'Тупо туалетная вода,наценка шо пипец,а не кто не берет...\r\nпечаль', 'Есть в наличии'),
(8, 8, 'куртка Четкая!!', 'Гопари и &ko', 10, 150, 'Отличные куртки в своем сигменте!', 'Есть в наличии'),
(9, 9, 'Туфли лабутены', 'Лабутен', 0, 150, 'Самые популярные туфли в текущем мясеце!', 'Нет в наличии'),
(10, 10, 'Телевизор', 'Sumsung', 1, 4500, 'Плазма,шнур,ЮСБ,просто Агонь', 'Есть в наличии');

-- --------------------------------------------------------

--
-- Структура таблицы `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(15) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `reg_date` date NOT NULL,
  `last_update` date NOT NULL,
  `status` enum('online','ofline','lock','something') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `User`
--

INSERT INTO `User` (`id`, `name`, `lastname`, `birthday`, `email`, `password`, `is_active`, `reg_date`, `last_update`, `status`) VALUES
(14, 'Olejon', 'Starling', '1991-10-12', 'wewr@eret.ru', '1234', 0, '2028-02-20', '2028-02-20', 'ofline'),
(2, 'Маша', 'Честновока', '1983-05-12', 'chectnokova', '6755235356', 1, '2016-01-01', '2016-02-26', 'ofline'),
(3, 'Степа', 'Хабибулин', '1973-12-04', 'habibulin@gmail.com', '346457412413', 1, '2015-04-13', '2016-01-12', 'online'),
(4, 'Полина', 'Кирвоградова', '1991-12-15', 'kirvogradova@lock.ru', '2543t32352', 0, '2005-12-12', '2016-02-09', 'ofline'),
(5, 'Владимир', 'Лимонов', '1985-11-06', 'limonov@htr.com', '45kjny5jyn', 0, '2009-12-01', '2016-02-21', 'ofline'),
(6, 'Анастасия', 'Халакова', '1995-11-23', 'halakova@lalala.com', 'k4jtb35bb', 1, '2005-11-12', '2016-02-22', 'online'),
(7, 'Евгения', 'Листокова', '1992-11-04', 'listokova@hahaha.ru', 'rg4444err', 0, '2010-01-15', '2016-02-01', 'lock'),
(8, 'Лена', 'Варварова', '2001-05-08', 'varvarova@rt.com', 'weg567654rfbtn', 0, '2014-01-15', '2016-02-22', 'online'),
(9, 'Петр', 'Лужков', '1970-01-18', 'luzkov@qq,com', '4lk6m4k6n', 0, '2014-01-15', '2015-12-28', 'something'),
(10, 'Алексей', 'Досков', '2016-02-23', 'gorohov@tt.com', '89898989', 1, '2015-12-01', '2016-02-01', 'lock'),
(15, 'Инокентий', 'Смартуновский', '2016-02-06', 'er@skn.com', '$2a$08$YjE1ZDky', 0, '2028-02-20', '2028-02-20', 'ofline');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
