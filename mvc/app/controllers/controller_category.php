<?php
class Controller_category extends Controller{

   public $tableName = 'category_products';
   public $content_view = 'category/update_view.php';

   function __construct()
   {
      $this->model = new Model_Category();
      $this->view = new  View();
   }

   function action_create()
   {
      $this->view->Generate('category/create_view.php', 'template_view.php');
      if (!empty($_POST)){
         $data = $this->model->create_category();
         if ($data!=0) {
            include "app/views/success_view.php";
         } else {
            include "app/views/failure_view.php";
         }
      }
   }
}