<?php


class Controller_product extends Controller{

    public $tableName = 'products';
    public $content_view = 'product/update_view.php';

    function __construct()
    {
        $this->model = new Model_Product();
        $this->view = new  View();
    }

    function action_create()
    {
        $this->view->Generate('product/create_view.php', 'template_view.php');
        if (!empty($_POST)){
            $data = $this->model->create_product();
            if ($data!=0) {
                include "app/views/success_view.php";
            } else {
                include "app/views/failure_view.php";
            }
        }
    }
}