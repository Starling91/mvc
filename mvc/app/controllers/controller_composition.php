<?php

class Controller_composition extends Controller{

   public $tableName = 'composition_order';
   public $content_view = 'composition/update_view.php';

   function __construct()
   {
      $this->model = new Model_Composition();
      $this->view = new  View();
   }

   function action_create()
   {
      $this->view->Generate('composition/create_view.php', 'template_view.php');
      if (!empty($_POST)){
         $data = $this->model->create_composition();
         if ($data!=0) {
            include "app/views/success_view.php";
         } else {
            include "app/views/failure_view.php";
         }
      }
   }
}