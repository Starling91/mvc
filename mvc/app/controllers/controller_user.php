<?php

class Controller_user extends Controller{

    public $tableName = 'User';
    public $content_view = 'user/update_view.php';

    function __construct()
    {
        $this->model = new Model_User();
        $this->view = new  View();
    }

    function action_create()
    {
        $this->view->Generate('user/create_view.php','template_view.php');
        if (!empty($_POST)){
            $data =$this->model->create_user();
            if ($data!=0) {
                include "app/views/success_view.php";
            } else {
                include "app/views/failure_view.php";
            }
        }
    }
    


}