<?php


class Controller_order extends Controller{

    public $tableName = 'Orders';
    public $content_view = 'order/update_view.php';

    function __construct()
    {
        $this->model = new Model_Order();
        $this->view = new  View();
    }

    function action_create()
    {
        $this->view->Generate('order/create_view.php','template_view.php');
        if (!empty($_POST)){
            $data =$this->model->create_order();
            if ($data!=0) {
                include "app/views/success_view.php";
            } else {
                include "app/views/failure_view.php";
            }
        }
    }
}