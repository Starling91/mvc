<?php

class Controller {

    public $model;
    public $view;
    public $tableName;
    public $content_view;
    
    function __construct()
    {
        $this->model = new Model();
        $this->view = new  View();
    }

    function action_index(){
        $data = $this->model->get_data($this->tableName);
        $this->view->Generate('table_view.php', 'template_view.php', $data);
    }
    
    function action_id(){
        $id = $_POST['id'];
        $data = $this->model->findOne($this->tableName, $id);
        $this->view->Generate('table_view.php', 'template_view.php', $data);
    }

    function action_delete(){
        $id = $_POST['id'];
        $data = $this->model->delete($this->tableName, $id);
        if ($data!=0) {
            $this->view->Generate('success_view.php', 'template_view.php');
        } else {
            $this->view->Generate('failure_view.php', 'template_view.php');
        }
    }
    

    function action_update(){

        $this->view->Generate($this->content_view, 'template_view.php');
        if (!empty($_POST)){
            $data =$this->model->update($this->tableName);
            if ($data!=0) {
                include "app/views/success_view.php";
            } else {
                include "app/views/failure_view.php";
            }
        }
    }
}