<?php
class  DB{

    public static function getConnection(){
        try{
            $pdo = new PDO('mysql:host=localhost; dbname=mywork','root','');
            $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            $pdo->exec('SET NAMES "utf8"');
            return $pdo;
        }
        catch (Exception $e){
            echo "ошибка при подключении Базы данных!". $e->getMessage();
            exit();
        }
    }

}