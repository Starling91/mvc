<?php if(isset($data)&&!empty($data)): ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <?php foreach ($data[0] as $column=>$row): ?>

                <th><?php echo $column ?></th>

            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $dataTable): ?>
            <tr>
                <?php foreach ($dataTable as $column1=>$row1): ?>

                    <td><?php echo $row1 ?></td>

                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="container">
        <a href="create/" class="btn btn-default btn-md" role="button">Добавить</a>
        <a href="update/" class="btn btn-default btn-md" role="button">Обновить</a>
        <a href="/" class="btn btn-default btn-md" id="main" role="button">На главную</a>
    </div>
    
    <div class="container">
        <form class="form-inline" role="form" method="post" action="id/">
            <div class="form-group">
                <label class="sr-only" for="search">Email</label>
                <input type="text" class="form-control" name="id" id="search" placeholder="Введите ID">
            </div>
            <button type="submit" class="btn btn-default">Найти</button>
        </form>
    </div>
    <div class="container">
        <form class="form-inline" role="form" method="post" action="delete/">
            <div class="form-group">
                <label class="sr-only" for="search">Email</label>
                <input type="text" class="form-control" name="id" id="search" placeholder="Введите ID">
            </div>
            <button type="submit" class="btn btn-default">Удалить</button>
        </form>
    </div>
<?php else: ?>
    <h1>Данные отсутствуют!</h1>
<?php endif; ?>

