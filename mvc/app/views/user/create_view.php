
<div class="container">
    <form class="form-horizontal" method="post" action="/user/create/" role="form">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Имя</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="name" name="name" placeholder="Введите имя">
            </div>
        </div>
        <div class="form-group">
            <label for="surname" class="col-sm-2 control-label">Фамилия</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="surname" name="surname" placeholder="Введите фамилию">
            </div>
        </div>
        <div class="form-group">
            <label for="birthday" class="col-sm-2 control-label">Дата рождения</label>
            <div class="col-sm-4">
                <input type="date" class="form-control" id="birthday" name="birthday" placeholder="Выберите дату">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-4">
                <input type="email" class="form-control" id="email" name="email" placeholder="Введите email">
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Пароль</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="password" name="password" placeholder="Введите пароль">
            </div>
        </div>
        <div class="form-group">
            <label for="is_active" class="col-sm-2 control-label">Статус активации</label>
            <div class="col-sm-4">
                <select class="form-control" name="active" id="is_active">
                    <option>1</option>
                    <option>0</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="reg_date" class="col-sm-2 control-label">Дата регистрации</label>
            <div class="col-sm-4">
                <input type="date" class="form-control" id="reg_date" name="reg" placeholder="Выберите дату регистрации">
            </div>
        </div>
        <div class="form-group">
            <label for="lust_update" class="col-sm-2 control-label">Дата последнего обновления</label>
            <div class="col-sm-4">
                <input type="date" class="form-control" id="lust_update" name="last" placeholder="Выберите дату последнего обновления">
            </div>
        </div>
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Статус</label>
            <div class="col-sm-4">
                <select class="form-control" name="status" id="status">
                    <option>online</option>
                    <option>offline</option>
                    <option>block</option>
                </select>
            </div>
        </div>
        <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">Добавить</button>
        </div>
      </div>
    </form>
</div>

