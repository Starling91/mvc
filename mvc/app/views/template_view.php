<!DOCTYPE html>
<!-- saved from url=(0046)http://bootstrap-3.ru/examples/justified-nav/# -->
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DB</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/Template/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/Template/justified-nav.css" rel="stylesheet">

</head>

<body>

<div class="container">

    <div class="masthead">
        <h3 class="text-muted">DataBase</h3>
        <ul class="nav nav-justified">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="/category/index">Category</a></li>
            <li><a href="/composition/index">Composition</a></li>
            <li><a href="/order/index">Orders</a></li>
            <li><a href="/product/index">Products</a></li>
            <li><a href="/user/index">Users</a></li>
        </ul>
    </div>

    <!-- Tables -->
<?php include "app/views/".$content_view; ?>

</body>
</html>
