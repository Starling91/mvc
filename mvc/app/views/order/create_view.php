<div class="container">
    <form class="form-horizontal" method="post" action="/order/create/" role="form">
        <div class="form-group">
            <label for="id_user" class="col-sm-2 control-label">ID пользователя</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="id_user" name="id_user" placeholder="Введите Id пользователя">
            </div>
        </div>
        <div class="form-group">
            <label for="date_order" class="col-sm-2 control-label">Дата заказа</label>
            <div class="col-sm-4">
                <input type="date" class="form-control" id="date_order" name="date_order" placeholder="Введите дату">
            </div>
        </div>
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Статус</label>
            <div class="col-sm-4">
                <select class="form-control" name="status" id="status">
                    <option>Принят</option>
                    <option>Доставлен</option>
                    <option>Обработан</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Добавить</button>
            </div>
        </div>
    </form>
</div>
