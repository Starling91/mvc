
<div class="container">
    <form class="form-horizontal" method="post" action="/category/create/" role="form">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Название категории</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="title" name="title" placeholder="Введите название">
            </div>
        </div>
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Статус</label>
            <div class="col-sm-4">
                <select class="form-control" name="status" id="status">
                    <option></option>
                    <option>Склад №1</option>
                    <option>Склад №2</option>
                    <option>Склад №3</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Добавить</button>
            </div>
        </div>
    </form>
</div>
