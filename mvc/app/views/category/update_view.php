<div class="container">
    <form class="form-horizontal" method="post" action="/category/update/" role="form">
        <div class="form-group">
            <label for="column" class="col-sm-2 control-label">Выберите колонку</label>
            <div class="col-sm-4">
                <select class="form-control" name="column" id="column">
                    <option>title</option>
                    <option>status</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="value" class="col-sm-2 control-label">Изменение данных</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="value" name="value" placeholder="Введите данные">
            </div>
        </div>

        <div class="form-group">
            <label for="value_find" class="col-sm-2 control-label">ID записи</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="value_find" name="value_find" placeholder="Введите ID ячейки">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Изменить</button>
            </div>
        </div>
    </form>
</div>