
<div class="container">
    <form class="form-horizontal" method="post" action="/product/create/" role="form">
        <div class="form-group">
            <label for="id_catalog" class="col-sm-2 control-label">ID каталога</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="id_catalog" name="id_catalog" placeholder="Введите Id каталога">
            </div>
        </div>
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Название товара</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="title" name="title" placeholder="Введите название">
            </div>
        </div>
        <div class="form-group">
            <label for="mark" class="col-sm-2 control-label">Марка</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="mark" name="mark" placeholder="Выберите марку">
            </div>
        </div>
        <div class="form-group">
            <label for="count" class="col-sm-2 control-label">Количество</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="count" name="count" placeholder="Введите количество">
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-sm-2 control-label">Цена</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="price" name="price" placeholder="Введите цену">
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Полное описание</label>
            <div class="col-sm-4">
                <textarea class="form-control" id="description" name="description" placeholder="Выберите полное описание">
                </textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="status" class="col-sm-2 control-label">Статус</label>
            <div class="col-sm-4">
                <select class="form-control" name="status" id="status">
                    <option>Есть в наличии</option>
                    <option>Нет в наличии</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Добавить</button>
            </div>
        </div>
    </form>
</div>
