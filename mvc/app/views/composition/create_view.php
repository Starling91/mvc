
<div class="container">
    <form class="form-horizontal" method="post" action="/composition/create/" role="form">
        <div class="form-group">
            <label for="id_order" class="col-sm-2 control-label">ID заказа</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="id_order" name="id_order" placeholder="Введите Id заказа">
            </div>
        </div>
        <div class="form-group">
            <label for="id_product" class="col-sm-2 control-label">ID продукта</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="id_product" name="id_product" placeholder="Введите Id продукта">
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-sm-2 control-label">Марка</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="price" name="price" placeholder="Введите сумму ">
            </div>
        </div>
        <div class="form-group">
            <label for="count" class="col-sm-2 control-label">Количество</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" id="count" name="count" placeholder="Введите количество">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Добавить</button>
            </div>
        </div>
    </form>
</div>
